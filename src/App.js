import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import "./styles//App.css";

const Main = React.lazy(() => import("./components/Main"));
const Other = React.lazy(() => import("./components/Other"));
const Another = React.lazy(() => import("./components/Another"));

const App = () => (
  <div className="App">
    <Router>
      <div>
        <ul className="Menu">
          <li>
            <Link to="/">Main</Link>
          </li>
          <li>
            <Link to="/2">Other</Link>
          </li>
          <li>
            <Link to="/3">Another</Link>
          </li>
        </ul>

        <hr />
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route exact path="/">
              <Main />
            </Route>
            <Route exact path="/2">
              <Other />
            </Route>
            <Route exact path="/3">
              <Another />
            </Route>
          </Switch>
        </Suspense>
      </div>
    </Router>
  </div>
);

export default App;
