import logo from "../images/logo.svg";

const Another = () => (
  <header className="App-header">
    <img src={logo} className="App-logo" alt="logo" />
    <p>Another Component</p>
  </header>
);

export default Another;
