import { render, screen } from "@testing-library/react";
import App from "./App";

describe("App Component", () => {
  test("renders Main react link", () => {
    render(<App />);
    const linkElement = screen.getByText(/Main/);
    expect(linkElement).toBeInTheDocument();
  });

  test("renders Another react link", () => {
    render(<App />);
    const linkElement = screen.getByText(/Another/);
    expect(linkElement).toBeInTheDocument();
  });

  test("renders Other react link", () => {
    render(<App />);
    const linkElement = screen.getByText(/Other/);
    expect(linkElement).toBeInTheDocument();
  });
});
